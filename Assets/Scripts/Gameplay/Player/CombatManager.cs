﻿using System.Collections;
using Core;
using Gameplay.Enemies;
using Gameplay.Input;
using UnityEngine;

namespace Gameplay
{
	public class CombatManager : InitializableBehaviour
	{
		private static readonly int Shoot = Animator.StringToHash("Shoot");
		
		[SerializeField] private InputManager inputManager = null;
		[SerializeField] private EnemyManager enemyManager = null;
		[SerializeField] private PowerUpManager powerUpManager = null;
		[SerializeField] private PlayerStatsManager playerStatsManager = null;
		[SerializeField] private Animator animator = null;
		[SerializeField] private AudioClip fireAudioClip = null;
		[SerializeField] private AudioClip hitAudioClip = null;
		[SerializeField] private LayerMask enemyMask = default;
		[SerializeField] private float raycastDistance = 10f;
		[SerializeField] private int maxEnemiesPerHit = 200;

		private int killCount = 0;
		private int autoFireSfxCounter = 0;
		private float timer = 0f;
		private float autoFireTimer = 0f;
		private RaycastHit2D[] hits = null;

		public bool IsOnFireHold { get; private set; } = false;
		public bool ShouldAutoFire => autoFireTimer > 0;
		public static CombatManager Instance { get; private set; } = null;

		public override void Initialize()
		{
			base.Initialize();

			Instance = this;

			timer = 0f;
			IsOnFireHold = false;
			inputManager.OnFire.Subscribe(OnFireCallback).AddTo(this);
			inputManager.OnFireHold.Subscribe(OnFireHoldCallback).AddTo(this);

			killCount = 0;
			hits = new RaycastHit2D[maxEnemiesPerHit];
		}

		public void Update()
		{
			if (!ShouldAutoFire)
			{
				autoFireTimer = 0;
				return;
			}

			if (!IsOnFireHold)
				return;
			timer += Time.deltaTime;
			autoFireTimer -= Time.deltaTime;

			if (timer >= 0.025f)
			{
				
				timer = 0f;
				animator.SetTrigger(Shoot);

				autoFireSfxCounter++;
				if (autoFireSfxCounter == 10)
				{
					autoFireSfxCounter = 0;
					AudioSource.PlayClipAtPoint(fireAudioClip, transform.position, 1.0f);
				}

				StartCoroutine(CheckIfEnemyHit());
			}
		}

		public void ResetFireHold()
		{
			IsOnFireHold = false;
		}

		public void IncreaseAutoFireTimer(float val)
		{
			autoFireTimer += val;
		}

		private void OnFireCallback(Unit unit)
		{
			if (ShouldAutoFire)
				return;
			
			animator.SetTrigger(Shoot);

			StartCoroutine(CheckIfEnemyHit());
			AudioSource.PlayClipAtPoint(fireAudioClip, transform.position, 1.0f);
		}

		private void OnFireHoldCallback(bool isOnHold)
		{
			IsOnFireHold = isOnHold;
		}

		private IEnumerator CheckIfEnemyHit()
		{
			int count = Physics2D.RaycastNonAlloc(transform.position, transform.right, this.hits, raycastDistance, enemyMask);

			int enemiesHit = 0;
			
			if (count > 0)
			{
				for (int i = 0; i < count; i++)
				{
					if (enemiesHit % 10 == 0)
						yield return null;
					
					//8 is the enemy layer
					if (hits[i].collider.gameObject.layer != 8)
					{
						break;
					}

					BaseEnemy enemy = hits[i].collider.GetComponent<BaseEnemy>();
					if (enemy.Health <= playerStatsManager.Damage.CurrentValue)
					{
						enemyManager.IncrementEnemiesToSpawn();
						GameManager.Instance.IncrementScore();
						killCount++;

						if (killCount >= 25)
						{
							powerUpManager.SpawnPowerUp();
							killCount = 0;
						}
					}
					hits[i].collider.GetComponent<BaseEnemy>().Hit(playerStatsManager.Damage.CurrentValue);
					enemiesHit++;
				}
				AudioSource.PlayClipAtPoint(hitAudioClip, transform.position, 1.0f);
			}
		}
	}
}