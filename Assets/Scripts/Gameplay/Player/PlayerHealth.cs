﻿using Core;
using Gameplay.Stats;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay
{
	public class PlayerHealth : InitializableBehaviour
	{
		[SerializeField] private Image hpFillBar = null;
		[SerializeField] private PlayerStatsManager statsManager = null;
		
		private HealthInt healthInt = null;

		public int Health => healthInt.CurrentValue;
		public HealthInt HealthInt => healthInt;
		
		public override void Initialize()
		{
			base.Initialize();

			healthInt = new HealthInt(0, 100, 100);
			hpFillBar.fillAmount = (float)Health / healthInt.MaxValue;
			healthInt.OnValueChanged.Subscribe(OnHealthChangedCallback).AddTo(this);
		}
		
		public void Damage(int damage)
		{
			if (Health <= 0)
				return;
			
			healthInt.Decrement(Mathf.FloorToInt(damage * (1 - statsManager.Defense.CurrentValue)));
		}

		private void OnHealthChangedCallback(int value)
		{
			hpFillBar.fillAmount = (float)Health / healthInt.MaxValue;
		}
	}
}