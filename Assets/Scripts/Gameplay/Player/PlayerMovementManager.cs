﻿using Core;
using Gameplay.Input;
using UnityEngine;

namespace Gameplay
{
	public class PlayerMovementManager : InitializableBehaviour
	{
		private static readonly int IsMoving = Animator.StringToHash("IsMoving");
		private static readonly int IsIdle = Animator.StringToHash("IsIdle");
		
		[SerializeField] private Camera cam = null;
		[SerializeField] private InputManager inputManager = null;
		[SerializeField] private PlayerStatsManager playerStatsManager = null;
		[SerializeField] private Rigidbody2D rigidBody2D = null;
		[SerializeField] private Animator animator = null;

		private Vector2 movementCache = default;
		private Vector2 targetCache = default;
		private Vector2 playerScreenPosCache = default;
		private bool isMoving = false;
		private bool isMovingLastFrame = false;
		

		public override void Initialize()
		{
			base.Initialize();

			inputManager.OnMove.Subscribe(OnPlayerMoveCallback).AddTo(this);
			inputManager.OnTarget.Subscribe(OnTargetCallback).AddTo(this);
		}
		
		private void FixedUpdate()
		{
			if (playerStatsManager.MoveSpeed == null)
				return;
			
			rigidBody2D.MovePosition(rigidBody2D.position + movementCache.normalized * (Time.fixedDeltaTime * playerStatsManager.MoveSpeed.CurrentValue));
			rigidBody2D.angularVelocity = 0;
		}

		private void OnPlayerMoveCallback(Vector2 value)
		{
			movementCache = value;
			isMovingLastFrame = isMoving;
			isMoving = movementCache.sqrMagnitude > 0;

			if (isMoving != isMovingLastFrame)
			{
				animator.SetBool(IsMoving, isMoving);
				animator.SetBool(IsIdle, !isMoving);
			}
		}

		private void OnTargetCallback(Vector2 value)
		{
			if (inputManager.Control == GameControl.KBM)
			{
				Vector3 pos = transform.position;
				pos.z = 10;
				playerScreenPosCache = cam.WorldToScreenPoint(pos);
				
				targetCache.x = value.x - playerScreenPosCache.x;
				targetCache.y = value.y - playerScreenPosCache.y;
			}
			else
			{
				targetCache = value;
			}
			
			if(targetCache.sqrMagnitude > 0)
				rigidBody2D.SetRotation(Mathf.Rad2Deg * Mathf.Atan2(targetCache.y, targetCache.x));

			rigidBody2D.angularVelocity = 0;
		}
	}
}