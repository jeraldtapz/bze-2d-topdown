﻿using Core;
using Gameplay.Stats;
using Sirenix.OdinInspector;

namespace Gameplay
{
	public class PlayerStatsManager : InitializableBehaviour
	{
		[ShowInInspector] public Damage Damage { get; private set; }
		[ShowInInspector] public Defense Defense { get; private set; }
		[ShowInInspector] public MoveSpeed MoveSpeed { get; private set; }

		public static PlayerStatsManager Instance { get; private set; }
		
		public override void Initialize()
		{
			base.Initialize();

			Instance = this;
			Damage = new Damage(0, int.MaxValue, 10);
			Defense = new Defense(0f, 1f, 0f);
			MoveSpeed = new MoveSpeed(0f, 100f, 12.5f);
		}
	}
}