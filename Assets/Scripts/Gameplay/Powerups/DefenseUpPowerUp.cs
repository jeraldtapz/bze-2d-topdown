﻿using System.Collections;
using Gameplay.Pool;
using UnityEngine;
using Utilities;

namespace Gameplay
{
	public class DefenseUpPowerUp : PowerUp
	{
		public override PowerUpType Type => PowerUpType.DefenseUp;
		
		private WaitForSeconds waitForSeconds = null;
		
		public override void Initialize()
		{
			base.Initialize();

			waitForSeconds ??= new WaitForSeconds(5f);
		}

		public override void Trigger()
		{
			base.Trigger();
			
			PlayerStatsManager.Instance.Defense.Increment(0.5f);
			
			CoroutineRunner.Instance.StartCoroutine(ResetDefense());

		}
		
		protected override void OnTriggerEnter2D(Collider2D col)
		{
			if (LayerUtils.IsOnInvalidLayer(PlayerLayerMask, col.gameObject.layer))
				return;
			
			Trigger();
			
			PoolManager.Instance.ReturnToPool(this);
		}


		private IEnumerator ResetDefense()
		{
			yield return waitForSeconds;
			PlayerStatsManager.Instance.Defense.Decrement(0.5f);
		}
	}
}