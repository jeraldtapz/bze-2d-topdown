﻿using System;
using Gameplay.GUI;
using Gameplay.Pool;
using UnityEngine;

namespace Gameplay
{
	public class PowerUp : Poolable
	{
		[SerializeField] protected LayerMask PlayerLayerMask = default;
 		
		public virtual PowerUpType Type { get; protected set; }

		public virtual void Trigger()
		{
			GUIManager.Instance.PickupPowerUp(Type);
		}

		protected virtual void OnTriggerEnter2D(Collider2D col)
		{
		}
	}

	public static class PowerUpExtensions
	{
		public static string PowerUpToString(this PowerUpType type)
		{
			switch (type)
			{

				case PowerUpType.AttackUp:    return "AttackUp";
				case PowerUpType.AutoFire:    return "AutoFire";
				case PowerUpType.DefenseUp:   return "DefenseUp";
				case PowerUpType.MoveSpeedUp: return "MoveSpeedUp";
				default:                      throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}
		
		public static string PowerUpToPrettyString(this PowerUpType type)
		{
			switch (type)
			{

				case PowerUpType.AttackUp:    return "Attack Up";
				case PowerUpType.AutoFire:    return "Auto Fire";
				case PowerUpType.DefenseUp:   return "Defense Up";
				case PowerUpType.MoveSpeedUp: return "Move Speed Up";
				default:                      throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}
	}
}