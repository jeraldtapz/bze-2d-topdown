﻿using System.Collections;
using Gameplay.Pool;
using UnityEngine;
using Utilities;

namespace Gameplay
{
	public class AttackUpPowerUp : PowerUp
	{
		public override PowerUpType Type => PowerUpType.AttackUp;

		private WaitForSeconds waitForSeconds = null;
		
		public override void Initialize()
		{
			base.Initialize();

			waitForSeconds ??= new WaitForSeconds(5f);
		}

		public override void Trigger()
		{
			base.Trigger();
			
			PlayerStatsManager.Instance.Damage.Increment(20);

			CoroutineRunner.Instance.StartCoroutine(ResetDamage());
		}

		protected override void OnTriggerEnter2D(Collider2D col)
		{
			if (LayerUtils.IsOnInvalidLayer(PlayerLayerMask, col.gameObject.layer))
				return;
			
			Trigger();
			
			PoolManager.Instance.ReturnToPool(this);
		}


		private IEnumerator ResetDamage()
		{
			yield return waitForSeconds;
			PlayerStatsManager.Instance.Damage.Decrement(20);
		}
	}
}