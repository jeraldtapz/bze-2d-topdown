﻿namespace Gameplay
{
	public enum PowerUpType
	{
		AttackUp,
		DefenseUp,
		MoveSpeedUp,
		AutoFire
	}
}