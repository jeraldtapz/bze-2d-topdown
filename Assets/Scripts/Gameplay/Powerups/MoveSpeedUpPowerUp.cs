﻿using System.Collections;
using Gameplay.Pool;
using UnityEngine;
using Utilities;

namespace Gameplay
{
	public class MoveSpeedUpPowerUp : PowerUp
	{
		public override PowerUpType Type => PowerUpType.MoveSpeedUp;
		
		private WaitForSeconds waitForSeconds = null;
		
		public override void Initialize()
		{
			base.Initialize();

			waitForSeconds ??= new WaitForSeconds(5f);
		}
		
		public override void Trigger()
		{
			base.Trigger();
			
			PlayerStatsManager.Instance.MoveSpeed.Increment(6f);

			CoroutineRunner.Instance.StartCoroutine(ResetMoveSpeed());
		}

		protected override void OnTriggerEnter2D(Collider2D col)
		{
			if (LayerUtils.IsOnInvalidLayer(PlayerLayerMask, col.gameObject.layer))
				return;
			
			Trigger();
			
			PoolManager.Instance.ReturnToPool(this);
		}


		private IEnumerator ResetMoveSpeed()
		{
			yield return waitForSeconds;
			PlayerStatsManager.Instance.MoveSpeed.Decrement(6f);
		}
	}
}