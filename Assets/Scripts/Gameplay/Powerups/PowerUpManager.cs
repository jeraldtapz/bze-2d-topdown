﻿using System.Collections.Generic;
using Core;
using Gameplay.Pool;
using UnityEngine;

namespace Gameplay
{
	public class PowerUpManager : InitializableBehaviour
	{
		[SerializeField] private PoolManager poolManager = null;
		[SerializeField] private Transform playerTransform = null;

		private List<PowerUp> powerUps = null;

		public override void Initialize()
		{
			base.Initialize();
			powerUps = new List<PowerUp>(20);
		}

		public override void DeInitialize()
		{
			base.DeInitialize();

			foreach (PowerUp powerUp in powerUps)
			{
				poolManager.ReturnToPool(powerUp);
			}
			
			powerUps.Clear();
			powerUps = null;
		}

		public void SpawnPowerUp()
		{
			PowerUpType type = (PowerUpType)Random.Range(0, 4);
			PowerUp powerUp = poolManager.Spawn<PowerUp>(type.PowerUpToString());
			powerUp.Initialize();
			powerUps.Add(powerUp);

			Vector2 pos = playerTransform.position;
			pos += Random.insideUnitCircle.normalized * 5;
			powerUp.transform.position = pos;
		}
	}
}