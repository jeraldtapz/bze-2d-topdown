﻿using System.Collections;
using Gameplay.Pool;
using UnityEngine;
using Utilities;

namespace Gameplay
{
	public class AutoFirePowerUp : PowerUp
	{
		public override PowerUpType Type => PowerUpType.AutoFire;

		private WaitForSeconds waitForSeconds = null;
		
		public override void Initialize()
		{
			base.Initialize();

			waitForSeconds ??= new WaitForSeconds(5f);
		}
		
		protected override void OnTriggerEnter2D(Collider2D col)
		{
			if (LayerUtils.IsOnInvalidLayer(PlayerLayerMask, col.gameObject.layer))
				return;
			
			Trigger();
			
			PoolManager.Instance.ReturnToPool(this);
		}
		
		public override void Trigger()
		{
			base.Trigger();
			
			CombatManager.Instance.IncreaseAutoFireTimer(5f);

			CoroutineRunner.Instance.StartCoroutine(ResetAutoFire());
		}

		private IEnumerator ResetAutoFire()
		{
			yield return waitForSeconds;
		}
	}
}