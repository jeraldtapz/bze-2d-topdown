﻿using Core;
using UnityEngine;

namespace Gameplay.Pool
{
	public class Poolable : InitializableBehaviour, IPoolable
	{
		[SerializeField] private string id = default;

		public string Id => id;
		
		public virtual void Spawn()
		{
			gameObject.SetActive(true);
		}
		public virtual void Despawn()
		{
			gameObject.SetActive(false);
		}
	}
}