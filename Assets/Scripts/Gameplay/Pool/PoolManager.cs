﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using HashUtilities = Utilities.HashUtilities;

namespace Gameplay.Pool
{
	public class PoolManager : InitializableBehaviour
	{
		[OdinSerialize] private List<PoolData> poolData = null;
		[SerializeField] private int maxSpawnPerFrame = 50;

		[ShowInInspector]
		private Dictionary<int, Stack<Poolable>> poolableMap = null;
		
		[ShowInInspector]
		private Dictionary<int, PoolEntry> poolEntriesInt = null;
		
		public static PoolManager Instance { get; private set; }
		
		public override IEnumerator InitializeAsync()
		{
			Instance = this;
			poolEntriesInt = new Dictionary<int, PoolEntry>();
			poolableMap = new Dictionary<int, Stack<Poolable>>();

			foreach (PoolData data in poolData)
			{
				Dictionary<string, PoolEntry> tempMap = data.PoolEntries;
				Dictionary<int, PoolEntry> tempMapInt = new Dictionary<int, PoolEntry>();
				
				foreach (KeyValuePair<string,PoolEntry> keyValuePair in tempMap)
				{
					tempMapInt.Add(HashUtilities.ToAnimatorHash(keyValuePair.Key) ,keyValuePair.Value);
				}

				foreach (KeyValuePair<int,PoolEntry> pair in tempMapInt)
				{
					int hash = pair.Key;
				
					Stack<Poolable> stack = new Stack<Poolable>();
					poolableMap[hash] = stack;

					int count = 0;
					for (int i = 0; i < pair.Value.InitialQuantity; i++)
					{
						Spawn(hash, pair.Value.Poolable);
						count++;
						if (count > maxSpawnPerFrame)
						{
							count = 0;
							yield return null;
						}
					}
					
					poolEntriesInt.Add(pair.Key, pair.Value);
				}
			}
		}

		public T Spawn<T>(string id) where T: Poolable
		{
			int hash = HashUtilities.ToAnimatorHash(id);
			if (!poolableMap.ContainsKey(hash))
				throw new KeyNotFoundException($"Poolable not found, {id}");
			
			IncreaseIfEmpty(hash, 10, poolEntriesInt[hash].Poolable);

			Poolable poolable = poolableMap[hash].Pop();

			if (poolable.Id != id)
				throw new Exception($"Spawn Id must be the same with poolable Id");
			
			poolable.Spawn();
				
			return (T)poolable;
		}

		public void ReturnToPool(Poolable poolable)
		{
			int hash = HashUtilities.ToAnimatorHash(poolable.Id);
			if (!poolableMap.ContainsKey(hash))
				throw new KeyNotFoundException($"Poolable not found, {poolable.Id}");
			
			poolable.Despawn();
			poolable.transform.SetParent(transform);
			
			poolableMap[hash].Push(poolable);
		}

		private void IncreaseIfEmpty(int id, int amount, Poolable poolable)
		{
			if (poolableMap[id].Count == 0)
			{
				for (int i = 0; i < amount; i++)
				{
					Spawn(id, poolable);
				}
			}
		}

		private void Spawn(int id, Poolable poolable)
		{
			Poolable p = Instantiate(poolable, transform);
			p.gameObject.SetActive(false);
			p.name = p.name + p.GetInstanceID().ToString();
			poolableMap[id].Push(p);
		}
	}
}