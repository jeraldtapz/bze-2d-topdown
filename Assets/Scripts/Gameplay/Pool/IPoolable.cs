﻿namespace Gameplay.Pool
{
	public interface IPoolable
	{
		string Id { get; }
		void Spawn();
		void Despawn();
	}
}