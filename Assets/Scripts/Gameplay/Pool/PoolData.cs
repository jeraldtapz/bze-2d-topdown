﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Gameplay.Pool
{
	[CreateAssetMenu(menuName = "Game/Pooling/Pool Data")]
	public class PoolData : SerializedScriptableObject
	{
		[OdinSerialize]
		public Dictionary<string, PoolEntry> PoolEntries;
	}
}