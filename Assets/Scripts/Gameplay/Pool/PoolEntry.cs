﻿using System;

namespace Gameplay.Pool
{
	[Serializable]
	public class PoolEntry
	{
		public int InitialQuantity;
		public Poolable Poolable;
	}
}