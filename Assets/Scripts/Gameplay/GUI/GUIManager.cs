﻿using System.Collections;
using Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.GUI
{
	public class GUIManager : InitializableBehaviour
	{
		[SerializeField] private GameObject gameOverPanel = null;
		[SerializeField] private GameObject pickupPanel = null;
		[SerializeField] private Button restartButton = null;
		[SerializeField] private Button quitButton = null;
		[SerializeField] private TextMeshProUGUI scoreText = null;
		[SerializeField] private TextMeshProUGUI pickupText = null;

		private float timer = 0f;
		private bool isPickupPanelActive = false;
		
		public static GUIManager Instance { get; private set; }

		public override void Initialize()
		{
			base.Initialize();

			Instance = this;
			
			gameOverPanel.SetActive(false);

			restartButton.onClick.AddListener(OnRestartCallback);
			quitButton.onClick.AddListener(OnQuitCallback);
			GameManager.Instance.Score.OnValueChanged.Subscribe(OnScoreChangedCallback).AddTo(this);
		}

		public override void DeInitialize()
		{
			base.DeInitialize();
			
			restartButton.onClick.RemoveListener(OnRestartCallback);
			quitButton.onClick.RemoveListener(OnQuitCallback);
		}

		public void EnableGameOverPanel()
		{
			gameOverPanel.SetActive(true);
		}

		public void PickupPowerUp(PowerUpType type)
		{
			pickupPanel.SetActive(true);

			timer = 3f;
			pickupText.text = type.PowerUpToPrettyString();

			if(!isPickupPanelActive)
				StartCoroutine(DisablePickupPanel());
		}

		private void OnRestartCallback()
		{
			gameOverPanel.SetActive(false);
			GameManager.Instance.RestartGame();
		}

		private void OnQuitCallback()
		{
			Application.Quit();
		}

		private void OnScoreChangedCallback(int score)
		{
			scoreText.text = $"Score: {score.ToString()}";
		}

		private IEnumerator DisablePickupPanel()
		{
			isPickupPanelActive = true;
			while (timer > 0)
			{
				timer -= Time.deltaTime;
				yield return null;
			}
			
			pickupPanel.SetActive(false);
			isPickupPanelActive = false;
		}
	}
}