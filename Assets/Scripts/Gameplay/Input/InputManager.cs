﻿using System;
using Core;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Gameplay.Input
{
	public class InputManager : InitializableBehaviour
	{
		private GameControls gameControls = null;

		private Subject<Unit> onFire = null;
		public IObservable<Unit> OnFire => onFire;
		private Subject<bool> onFireHold = null;
		public IObservable<bool> OnFireHold => onFireHold;

		private Subject<Vector2> onMove = null;
		public IObservable<Vector2> OnMove => onMove;
		
		private Subject<Vector2> onTarget = null;
		public IObservable<Vector2> OnTarget => onTarget;

		private Subject<GameControl> onControlChanged = null;
		public IObservable<GameControl> OnControlChanged => onControlChanged;
		
		public GameControl Control { get; private set; }
		
		public override void Initialize()
		{
			base.Initialize();
			
			gameControls = new GameControls();
			onFire = new Subject<Unit>();
			onFireHold = new Subject<bool>();
			onMove = new Subject<Vector2>();
			onTarget = new Subject<Vector2>();
			onControlChanged = new Subject<GameControl>();

			gameControls.Gameplay.Fire.performed += OnFireCallback;
			gameControls.Gameplay.FirePress.performed += OnFirePressCallback;
			gameControls.Gameplay.FireRelease.performed += OnFireReleaseCallback;
			gameControls.Gameplay.Movement.performed += OnMoveCallback;
			gameControls.Gameplay.Target.performed += OnTargetCallback;
			
			gameControls.Gameplay.Fire.performed += CheckForControlChangeCallback;
			gameControls.Gameplay.FirePress.performed += CheckForControlChangeCallback;
			gameControls.Gameplay.FireRelease.performed += CheckForControlChangeCallback;
			gameControls.Gameplay.Movement.performed += CheckForControlChangeCallback;
			gameControls.Gameplay.Target.performed += CheckForControlChangeCallback;
			
			gameControls.Gameplay.Enable();
		}

		public override void DeInitialize()
		{
			base.DeInitialize();

			gameControls.Gameplay.Fire.performed -= OnFireCallback;
			gameControls.Gameplay.FirePress.performed -= OnFirePressCallback;
			gameControls.Gameplay.FireRelease.performed -= OnFireReleaseCallback;
			gameControls.Gameplay.Movement.performed -= OnMoveCallback;
			gameControls.Gameplay.Target.performed -= OnTargetCallback;

			gameControls.Gameplay.Fire.performed -= CheckForControlChangeCallback;
			gameControls.Gameplay.FirePress.performed -= CheckForControlChangeCallback;
			gameControls.Gameplay.FireRelease.performed -= CheckForControlChangeCallback;
			gameControls.Gameplay.Movement.performed -= CheckForControlChangeCallback;
			gameControls.Gameplay.Target.performed -= CheckForControlChangeCallback;

			gameControls = null;
		}

		private void OnFireCallback(InputAction.CallbackContext context)
		{
			onFire.OnNext(Unit.Default);
		}
		
		private void OnFirePressCallback(InputAction.CallbackContext context)
		{
			onFireHold.OnNext(true);
		}
		
		private void OnFireReleaseCallback(InputAction.CallbackContext context)
		{
			onFireHold.OnNext(false);
		}

		private void OnMoveCallback(InputAction.CallbackContext context)
		{
			onMove.OnNext(context.ReadValue<Vector2>());
		}
		
		private void OnTargetCallback(InputAction.CallbackContext context)
		{
			onTarget.OnNext(context.ReadValue<Vector2>());
		}

		private void CheckForControlChangeCallback(InputAction.CallbackContext context)
		{
			if (Control == GameControl.Gamepad)
			{
				if (context.action.activeControl?.device.name == "Keyboard" || context.action.activeControl?.device.name == "Mouse")
				{
					Control = GameControl.KBM;
					onControlChanged.OnNext(Control);
				}
			}
			else
			{
				if (context.action.activeControl?.device.name != "Keyboard" && context.action.activeControl?.device.name != "Mouse")
				{
					Control = GameControl.Gamepad;
					onControlChanged.OnNext(Control);
				}
			}
		}
	}
}