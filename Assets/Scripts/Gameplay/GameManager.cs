﻿using System;
using System.Collections;
using Core;
using Core.Grid;
using Gameplay.Enemies;
using Gameplay.GUI;
using Gameplay.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gameplay
{
	public class GameManager : InitializableBehaviour
	{
		[SerializeField] private GridManager gridManager = null;
		[SerializeField] private GUIManager guiManager = null;
		[SerializeField] private EnemyManager enemyManager = null;
		[SerializeField] private CombatManager combatManager = null;
		[SerializeField] private PlayerMovementManager playerMovementManager = null;
		[SerializeField] private PlayerHealth playerHealth = null;
		[SerializeField] private PowerUpManager powerUpManager = null;

		private IDisposable healthChangedDisposable = null;

		public Score Score { get; private set; } = null;
		public static GameManager Instance { get; private set; }

		public override void Initialize()
		{
			base.Initialize();
			Score = new Score(0, int.MaxValue, 0);
			Instance = this;
			
			healthChangedDisposable = playerHealth.HealthInt.OnValueChanged.Subscribe(OnPlayerHealthChangedCallback);
		}

		[Button]
		public IEnumerator DeInitializeGame()
		{
			yield return null;
			
			gridManager.DeInitialize();
			enemyManager.DeInitialize();
			combatManager.DeInitialize();
			playerMovementManager.DeInitialize();
			playerHealth.DeInitialize();
			powerUpManager.DeInitialize();
		}

		[Button]
		public void RestartGame()
		{
			gridManager.Initialize();
			enemyManager.Initialize();
			combatManager.Initialize();
			playerMovementManager.Initialize();
			playerHealth.Initialize();
			powerUpManager.Initialize();
			
			Score.Set(0);
			healthChangedDisposable?.Dispose();
			healthChangedDisposable = playerHealth.HealthInt.OnValueChanged.Subscribe(OnPlayerHealthChangedCallback);
		}

		public void IncrementScore()
		{
			Score.Set(Score.CurrentValue + 1);
		}

		private void OnPlayerHealthChangedCallback(int value)
		{
			if (value <= 0)
			{
				guiManager.EnableGameOverPanel();
				StartCoroutine(DeInitializeGame());
			}
		}
	}
}