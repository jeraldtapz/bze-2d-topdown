﻿using System;
using Core;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Gameplay
{
	public class Bootstrap : InitializableBehaviour
	{
		[OdinSerialize] [NonSerialized] private List<IInitializable> initializables = null;
		[SerializeField] private bool initializeOnAwake = false;
 		
		private void Awake()
		{
			if(initializeOnAwake)
				StartCoroutine(InitializeAsync());
		}

		[Button]
		public override void Initialize()
		{
			base.Initialize();
			
			StartCoroutine(InitializeAsync());
		}

		public override IEnumerator InitializeAsync()
		{
			Cursor.lockState = CursorLockMode.Confined;
			
			foreach (IInitializable initializable in initializables)
			{
				yield return StartCoroutine(initializable.InitializeAsync());
			}
		}

		public override IEnumerator DeInitializeAsync()
		{
			foreach (IInitializable initializable in initializables)
			{
				yield return StartCoroutine(initializable.DeInitializeAsync());
			}
		}
	}
}