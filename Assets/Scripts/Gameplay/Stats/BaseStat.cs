﻿using System;
using Core;

namespace Gameplay.Stats
{
	public class BaseStat<T> : IStat<T> 
	{
		protected readonly Subject<T> OnValueChangedSubject = null;
		
		public T CurrentValue { get; protected set; }
		public T MaxValue { get; protected set; }
		public T MinValue { get; protected set; }
		public IObservable<T> OnValueChanged => OnValueChangedSubject;

		public BaseStat(T min, T max, T initialValue)
		{
			OnValueChangedSubject = new Subject<T>();
			MinValue = min;
			MaxValue = max;
			CurrentValue = initialValue;
		}
		
		public virtual void Set(T value)
		{
			CurrentValue = value;

			OnValueChangedSubject.OnNext(CurrentValue);
		}
		
		public virtual void Increment(T value)
		{
			throw new NotImplementedException();
		}
		
		public virtual void Decrement(T value)
		{
			throw new NotImplementedException();
		}
	}
}