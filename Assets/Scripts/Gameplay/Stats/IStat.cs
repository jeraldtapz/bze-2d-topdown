﻿using System;

namespace Gameplay.Stats
{
	public interface IStat <T>
	{
		T CurrentValue { get; }
		T MaxValue { get; }
		T MinValue { get; }
		IObservable<T> OnValueChanged { get; }
		void Set(T value);
		void Increment(T value);
		void Decrement(T value);
	}
}