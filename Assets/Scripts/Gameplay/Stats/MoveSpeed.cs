﻿using UnityEngine;

namespace Gameplay.Stats
{
	public class MoveSpeed : BaseStat<float>
	{
		public MoveSpeed(float min, float max, float initialValue) : base(min, max, initialValue)
		{
		}
		
		public override void Set(float value)
		{
			CurrentValue = value;
			CurrentValue = Mathf.Clamp(CurrentValue, MinValue, MaxValue);
			OnValueChangedSubject.OnNext(CurrentValue);
		}

		public override void Increment(float value)
		{
			Set(CurrentValue + value);
		}

		public override void Decrement(float value)
		{
			Set(CurrentValue - value);
		}
	}
}