﻿using UnityEngine;

namespace Gameplay.Stats
{
	public class Score : BaseStat<int>
	{
		public Score(int min, int max, int initialValue) : base(min, max, initialValue)
		{
		}
		
		public override void Set(int value)
		{
			CurrentValue = value;
			CurrentValue = Mathf.Clamp(CurrentValue, MinValue, MaxValue);
			OnValueChangedSubject.OnNext(CurrentValue);
		}

		public override void Increment(int value)
		{
			Set(CurrentValue + value);
		}

		public override void Decrement(int value)
		{
			Set(CurrentValue - value);
		}
	}
}