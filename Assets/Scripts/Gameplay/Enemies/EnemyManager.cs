﻿using System.Collections;
using System.Collections.Generic;
using Core;
using Core.Grid;
using Gameplay.Pool;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameplay.Enemies
{
	public class EnemyManager : InitializableBehaviour
	{
		[SerializeField] private int enemyCount = 1000;
		[SerializeField] private int initialSpawn = 100;
		[SerializeField] private float secondsPerSpawn = 0.5f;
		[SerializeField] private int minimumTargetFrameRate = 60;
		[SerializeField] private float secondsPerEnemyTick = 1f;
		[SerializeField] private PoolManager poolManager = null;
		[SerializeField] private Transform playerTransform = null;
		[SerializeField] private Camera cam = null;
		[SerializeField] private GridData gridData = null;

		[ShowInInspector]
		private List<BaseEnemy> activeEnemies = null;
		private HashSet<int> activeEnemiesHash = null;
		private WaitForSeconds waitForSeconds = null;
		private Coroutine spawnEnemiesCoroutine = null;
		private bool shouldProcessEnemies = false;
		private int enemyIndex = 0;
		private int enemiesLeftToSpawn = 0;
		private int enemiesToProcessPerFrame = 0;
		private int enemyCountTracker = 0;
		private float secondsPerFrame = 0;
		private float timer = 0f;
		private List<int> indicesToRemoveCache = null;
		private Vector2 edge = default;
		
		public override void Initialize()
		{
			base.Initialize();
			
			indicesToRemoveCache = new List<int>(5);
			activeEnemies = new List<BaseEnemy>(enemyCount);
			activeEnemiesHash = new HashSet<int>();
			waitForSeconds = new WaitForSeconds(secondsPerSpawn);

			enemiesToProcessPerFrame = Mathf.CeilToInt(enemyCount / (minimumTargetFrameRate * secondsPerEnemyTick));
			secondsPerFrame = 1f / minimumTargetFrameRate;
			
			StartCoroutine(SpawnInitialEnemies());
			spawnEnemiesCoroutine = StartCoroutine(SpawnEnemyBacklog());
		}

		public override void DeInitialize()
		{
			base.DeInitialize();
			shouldProcessEnemies = false;
			enemiesLeftToSpawn = 0;
			enemyIndex = 0;
			enemyCountTracker = 0;
			timer = 0f;
			StopCoroutine(spawnEnemiesCoroutine);

			foreach (BaseEnemy activeEnemy in activeEnemies)
			{
				if(activeEnemy.Health > 0)
					poolManager.ReturnToPool(activeEnemy);
			}
			activeEnemies.Clear();
		}

		private void Update()
		{
			if(!shouldProcessEnemies)
				return;

			timer += Time.deltaTime;
			if (timer < secondsPerFrame)
			{
				return;
			}

			timer = 0;
			int count = activeEnemies.Count;
			
			for (int i = 0; i < enemiesToProcessPerFrame; i++)
			{
				activeEnemies[enemyIndex].Tick();
				
				enemyIndex++;

				if (enemyIndex == count)
				{
					enemyIndex = 0;
				}
			}
		}

		private IEnumerator SpawnInitialEnemies()
		{
			edge.x = cam.aspect * gridData.OrthographicSize * gridData.GridX - 4f;
			edge.y = gridData.OrthographicSize * gridData.GridY - 4f;

			for (int i = 0; i < initialSpawn; i++)
			{
				enemyCountTracker = i;
				BaseEnemy enemy = SpawnEnemy();
				enemy.transform.SetParent(transform);
				enemy.SpriteRenderer.sortingOrder = 100 + enemyCountTracker;
				enemy.gameObject.name = $"Enemy {enemyCountTracker}";

				Vector2 pos = GetSpawnPos(edge);

				enemy.transform.position = pos;
				
				yield return enemy.InitializeAsync();
				enemy.SetTarget(playerTransform);
				enemy.Tick();
				
				if (!activeEnemiesHash.Contains(enemy.GetHashCode()))
				{
					activeEnemiesHash.Add(enemy.GetHashCode());
					activeEnemies.Add(enemy);
				}

				yield return null;
			}
			
			shouldProcessEnemies = true;
		}

		private IEnumerator SpawnEnemyBacklog()
		{
			enemiesLeftToSpawn = enemyCount - initialSpawn;
			while (true)
			{
				if (enemiesLeftToSpawn <= 0)
				{
					yield return null;
					continue;
				}
				
				enemyCountTracker++;
				if (enemyCountTracker == enemyCount)
					enemyCountTracker = 0;

				BaseEnemy enemy = SpawnEnemy();
				enemy.transform.SetParent(transform);
				enemy.SpriteRenderer.sortingOrder = 100 + initialSpawn + enemyCountTracker;
				enemy.gameObject.name = $"Enemy {initialSpawn + enemyCountTracker}";
				
				Vector2 pos = GetSpawnPos(edge);

				enemy.transform.position = pos;
				
				yield return enemy.InitializeAsync();
				enemy.SetTarget(playerTransform);
				enemy.Tick();
				
				if (!activeEnemiesHash.Contains(enemy.GetHashCode()))
				{
					activeEnemiesHash.Add(enemy.GetHashCode());
					activeEnemies.Add(enemy);
				}
				
				yield return waitForSeconds;

				enemiesLeftToSpawn--;
			}
		}

		public void IncrementEnemiesToSpawn()
		{
			enemiesLeftToSpawn++;
		}
		
		private static Vector2 GetSpawnPos(Vector2 edge)
		{

			Vector2 pos;
			bool useX = Random.Range(0f, 1f) > 0.5f;

			if (useX)
			{
				bool isRight = Random.Range(0f, 1f) > 0.5f;

				pos.y = Random.Range(-edge.y, edge.y);

				if (isRight)
				{
					pos.x = Random.Range(edge.x - 1f, edge.x + 1f);
				}
				else
				{
					pos.x = Random.Range(-edge.x - 1f, -edge.x + 1f);
				}
			}
			else
			{
				bool isUp = Random.Range(0f, 1f) > 0.5f;

				pos.x = Random.Range(-edge.x, edge.x);

				if (isUp)
				{
					pos.y = Random.Range(edge.y - 1f, edge.y + 1f);
				}
				else
				{
					pos.y = Random.Range(-edge.y - 1f, -edge.y + 1f);
				}
			}
			return pos;
		}

		private BaseEnemy SpawnEnemy()
		{
			return poolManager.Spawn<BaseEnemy>($"Zombie{Random.Range(1, 5)}");
		}
	}
}