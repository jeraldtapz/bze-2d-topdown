﻿using System.Collections;
using Gameplay.Pool;
using Gameplay.Stats;
using JetBrains.Annotations;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

namespace Gameplay.Enemies
{
	public class BaseEnemy : Poolable
	{
		private static readonly int AttackHash = Animator.StringToHash("Attack");
		private static readonly int IsMoving = Animator.StringToHash("IsMoving");
		private static readonly int IsIdle = Animator.StringToHash("IsIdle");
		
		[SerializeField] private SpriteRenderer spriteRenderer = null;
		[SerializeField] private Animator animator = null;
		[SerializeField] private Rigidbody2D rigidBody2D = null;
		[SerializeField] private EnemyData enemyData = null;
		[SerializeField] private LayerMask environmentLayerMask = default;
		[SerializeField] private LayerMask pLayerLayerMask = default;
		
		protected float DistanceToAttackSquared = 0f;
		protected float LastAttackTime = 0;
		protected Transform Target = null;
		protected HealthInt HealthInt = null;
		private RaycastHit2D[] hits = null;
		
		public SpriteRenderer SpriteRenderer => spriteRenderer;
		public int Health => HealthInt.CurrentValue;
		
		public override void Initialize()
		{
			base.Initialize();

			hits = new RaycastHit2D[2];
			HealthInt = new HealthInt(0, enemyData.HP, enemyData.HP);
			DistanceToAttackSquared = enemyData.DistanceToAttack * enemyData.DistanceToAttack;
		}

		public void Tick()
		{
			Vector2 dirToTarget = (Target.position - transform.position);
			Vector2 normDirToTarget = dirToTarget.normalized;
			
			int count = Physics2D.CircleCastNonAlloc(transform.position, enemyData.CheckRadius, transform.right, hits,
				enemyData.CheckDistance, environmentLayerMask);

			if (count > 0)
			{
				transform.rotation *= Quaternion.AngleAxis(Random.Range(45f, 90f), Vector3.forward);
			}
			else
			{
				float angle = Mathf.Atan2(normDirToTarget.y, normDirToTarget.x) * Mathf.Rad2Deg;
				transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
				transform.rotation *= Quaternion.FromToRotation(transform.right, normDirToTarget);
			}
			
			if (dirToTarget.sqrMagnitude < DistanceToAttackSquared)
			{
				if (Time.time - LastAttackTime > enemyData.AttackCooldown)
				{
					Attack();
					rigidBody2D.velocity = Vector2.zero;
				}
				else
				{
					animator.SetBool(IsIdle, true);
					animator.SetBool(IsMoving, false);
				}
			}
			else
			{
				normDirToTarget.x += Random.Range(-0.1f, 0.1f);
				normDirToTarget.y += Random.Range(-0.1f, 0.1f);
				rigidBody2D.velocity = Random.Range(0.9f, 1.1f) * enemyData.MoveSpeed * transform.right;
				animator.SetBool(IsMoving, true);
				animator.SetBool(IsIdle, false);
			}
		}
		
		public void SetTarget(Transform t)
		{
			Target = t;
		}

		public void Hit(int damage)
		{
			if (Health <= 0)
				return;
			
			HealthInt.Decrement(damage);

			if (Health <= 0)
			{
				PoolManager.Instance.ReturnToPool(this);
				BloodVfx bloodVfx = PoolManager.Instance.Spawn<BloodVfx>("Blood");
				bloodVfx.transform.position = transform.position;

				CoroutineRunner.Instance.StartCoroutine(DespawnBloodAfterDelay(bloodVfx));
			}
			else
			{
				rigidBody2D.velocity = transform.right * -4f;
			}
		}

		protected virtual void Attack()
		{
			LastAttackTime = Time.time;
			animator.SetTrigger(AttackHash);		
			animator.SetBool(IsMoving, false);
			animator.SetBool(IsIdle, true);
		}

		private IEnumerator DespawnBloodAfterDelay(BloodVfx bloodVfx)
		{
			float timer = 0f;
			while (timer < 2f)
			{
				timer += Time.deltaTime;
				bloodVfx.SetAlpha(Mathf.Clamp(2 - timer, 0, 1));				
				yield return null;
			}
			
			PoolManager.Instance.ReturnToPool(bloodVfx);
		}

		[UsedImplicitly]
		public void OnAttackEndCallback()
		{
			int count = Physics2D.CircleCastNonAlloc(transform.position, enemyData.CheckRadius, transform.right, hits,
				2, pLayerLayerMask);

			if (count > 0)
			{
				hits[0].collider.transform.parent.GetComponent<PlayerHealth>().Damage(enemyData.Damage);
			}
		}

#if UNITY_EDITOR

		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.yellow;
			Matrix4x4 oldMatrix = Gizmos.matrix;
			Gizmos.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(1, 1, 0.01f));
			Gizmos.DrawWireSphere(transform.position, enemyData.CheckRadius);
			Gizmos.DrawWireSphere(transform.position + transform.right * enemyData.CheckDistance, enemyData.CheckRadius);
			
			Gizmos.color = Color.black;
			Gizmos.matrix = oldMatrix;
			Gizmos.DrawLine(transform.position, transform.position + transform.right * enemyData.CheckDistance);
		}

#endif
	}
}