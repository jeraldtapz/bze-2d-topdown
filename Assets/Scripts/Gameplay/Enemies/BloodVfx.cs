﻿using Gameplay.Pool;
using UnityEngine;

namespace Gameplay.Enemies
{
	public class BloodVfx : Poolable
	{
		[SerializeField] private SpriteRenderer spriteRenderer = null;
		
		public override void Spawn()
		{
			base.Spawn();

			Color color = spriteRenderer.color;
			color.a = 1f;

			spriteRenderer.color = color;
			
			transform.Rotate(0, 0, Random.Range(0, 360));
		}

		public void SetAlpha(float a)
		{
			Color color = spriteRenderer.color;
			color.a = a;

			spriteRenderer.color = color;
		}
	}
}