﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Gameplay.Enemies
{
	[CreateAssetMenu(menuName = "Game/Enemy/Enemy Data")]
	public class EnemyData : SerializedScriptableObject
	{
		public string Name = "";
		[Range(0, 500)]    
		public int Damage = 10;
		
		[Range(10, 5000)]   
		public int HP = 100;
		
		[Range(1f, 20f)]   
		public float MoveSpeed = 5f;
		
		public float DistanceToAttack = 1f;
		public float AttackCooldown = 1f;
		public float CheckRadius = 0.85f;
		public float CheckDistance = 1f;
	}
}