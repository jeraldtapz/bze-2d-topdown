﻿using System;

namespace Core
{
	public class SubjectObserver<T> : IObserver<T>
	{
		private readonly Action<T> onNext;
		private readonly Action<Exception> onError;
		private readonly Action onCompleted;

		public SubjectObserver(Action<T> onNext, Action<Exception> onError = null, Action onCompleted = null)
		{
			this.onNext = onNext;
			this.onError = onError;
			this.onCompleted = onCompleted;
		}

		public void OnNext(T value)
		{
			onNext?.Invoke(value);
		}

		public void OnError(Exception error)
		{
			onError?.Invoke(error);
		}

		public void OnCompleted()
		{
			onCompleted?.Invoke();
		}
	}
}