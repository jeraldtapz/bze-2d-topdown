﻿using System;
using System.Collections.Generic;

namespace Core
{
    public class Subject<T> : IObservable<T>, IObserver<T>, IDisposable
    {
        private readonly object observerLock = new object();
        public readonly List<IObserver<T>> Observers = null;
        private bool isDisposed;
        private bool isStopped;

        public Subject()
        {
            Observers = new List<IObserver<T>>();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (observer == null)
                throw new ArgumentNullException(nameof(observer));

            lock (observerLock)
            {
                ThrowIfDisposed();

                if (isStopped) throw new Exception("This subject has had an error or has been completed before!");

                if (!Observers.Contains(observer))
                {
                    Observers.Add(observer);
                }
            }

            return new Subscription(this, observer);
        }

        public void OnNext(T value)
        {
            lock (observerLock)
            {
                ThrowIfDisposed();
                if (isStopped) return;

                foreach (IObserver<T> observer in Observers)
                {
                    observer.OnNext(value);
                }
            }
        }

        public void OnError(Exception error)
        {
            if (error == null)
                throw new ArgumentNullException(nameof(error));

            lock (observerLock)
            {
                ThrowIfDisposed();

                if (isStopped)
                    return;

                foreach (IObserver<T> observer in Observers)
                {
                    observer.OnError(error);
                }

                isStopped = true;
            }
        }

        public void OnCompleted()
        {
            lock (observerLock)
            {
                ThrowIfDisposed();

                if (isStopped)
                    return;

                foreach (IObserver<T> observer in Observers)
                {
                    observer.OnCompleted();
                }
                isStopped = true;
            }
        }

        private void ThrowIfDisposed()
        {
            if (isDisposed)
                throw new Exception("The Subject instance is already disposed!");
        }

        public void Dispose()
        {
            lock (observerLock)
            {
                isDisposed = true;
            }
        }

        internal class Subscription : IDisposable
        {
            private readonly object lockObject = new object();
            private Subject<T> observable;
            private IObserver<T> observer;
        
            public Subscription(Subject<T> observable, IObserver<T> observer)
            {
                this.observer = observer;
                this.observable = observable;
            }
        
            public void Dispose()
            {
                lock (lockObject)
                {
                    if (observable != null)
                    {
                        lock (observable.observerLock)
                        {
                            observable.Observers.Remove(observer);
        
                            observable = null;
                            observer = null;
                        }
                    }
                }
            }
        }
    }
}