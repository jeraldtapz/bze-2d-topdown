﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Core
{
	public class InitializableBehaviour : SerializedMonoBehaviour, IInitializable
	{
		[SerializeField] private bool shouldInitializeAsync = false;

		protected List<IDisposable> DisposablesList = null;
		protected WaitForEndOfFrame WaitForEndOfFrame = null;
		
		
		public List<IDisposable> Disposables => DisposablesList;

		public bool ShouldInitializeAsync => shouldInitializeAsync;
		
		public virtual void Initialize()
		{
			DisposablesList = new List<IDisposable>();
			WaitForEndOfFrame = new WaitForEndOfFrame();
		}
		
		public virtual void DeInitialize()
		{
			foreach (IDisposable disposable in Disposables)
			{
				disposable.Dispose();
			}
			
			DisposablesList.Clear();
			DisposablesList = null;
		}
		
		public virtual IEnumerator InitializeAsync()
		{
			Initialize();
			yield break;
		}
		
		public virtual IEnumerator DeInitializeAsync()
		{
			DeInitialize();
			yield break;
		}
	}

	public static class InitializableExtensions
	{
		public static void AddTo(this IDisposable disposable, InitializableBehaviour behaviour)
		{
			behaviour.Disposables.Add(disposable);
		}
	}
}