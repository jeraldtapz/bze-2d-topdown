﻿using System;

namespace Core
{
    public static class ObservableExtensions
    {
        public static IDisposable Subscribe<T>(this IObservable<T> observable, Action<T> onNext, Action<Exception> onError = null, Action onCompleted = null)
        {
            return observable.Subscribe(new SubjectObserver<T>(onNext, onError, onCompleted));
        }
    }
}