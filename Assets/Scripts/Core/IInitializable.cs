﻿using System.Collections;

namespace Core
{
	public interface IInitializable
	{
		bool ShouldInitializeAsync { get; }
		
		void Initialize();
		void DeInitialize();
		IEnumerator InitializeAsync();
		IEnumerator DeInitializeAsync();
	}
}