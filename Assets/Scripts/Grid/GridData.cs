﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Core.Grid
{
	[CreateAssetMenu(menuName = "Game/Grid/GridData")]
	public class GridData : SerializedScriptableObject
	{
		public Sprite[] GroundTileSpriteList;
		public GameObject[] EnvironmentElements;
		public float CameraDistance = 10f;
		public float OrthographicSize = 10f;
		public int GridX = 3;
		public int GridY = 3;
		public int ElementsToSpawn = 20;

		public Sprite GetRandomGroundTile()
		{
			return GroundTileSpriteList[Random.Range(0, GroundTileSpriteList.Length)];
		}

		public GameObject GetRandomEnvironmentElement()
		{
			return EnvironmentElements[Random.Range(0, EnvironmentElements.Length)];

		}
	}
}