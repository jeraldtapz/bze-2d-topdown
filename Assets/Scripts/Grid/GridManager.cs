using System.Collections;
using Cinemachine;
using UnityEngine;

namespace Core.Grid
{
    public class GridManager : InitializableBehaviour
    {
        private static readonly int Tiling = Shader.PropertyToID("_Tiling");

        [SerializeField] private CinemachineVirtualCamera vCam = null;
        [SerializeField] private Camera cam = null;
        [SerializeField] private Collider2D cameraConfiner = null;
        [SerializeField] private GameObject colliderPrefab = null;
        [SerializeField] private GridData gridData = null;
        [SerializeField] private SpriteRenderer gridSpriteRenderer = null;


        private Transform[] borderColliders = null;
        private GameObject[] environmentElements = null;
        
        public override void Initialize()
        {
            base.Initialize();

            borderColliders = new Transform[4];
            
            SetGridInitialSize();
        }

        public override void DeInitialize()
        {
            base.DeInitialize();

            foreach (GameObject obj in environmentElements)
            {
                Destroy(obj);
            }

            foreach (Transform borderCollider in borderColliders)
            {
                Destroy(borderCollider.gameObject);
            }
        }

        public void SetGridInitialSize()
        {
            SetGroundScale();

            StartCoroutine(SpawnEnvironmentElements());
        }
        
        private void SetGroundScale()
        {
            Vector3 scale = Vector3.zero;
            float aspectTimesOrthoSize = cam.aspect * gridData.OrthographicSize;

            scale.z = 1;
            scale.y = gridData.OrthographicSize * 2f * gridData.GridY;
            scale.x = aspectTimesOrthoSize * 2f * gridData.GridX;
            gridSpriteRenderer.transform.localScale = scale;

            MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();
            gridSpriteRenderer.GetPropertyBlock(propertyBlock);
            propertyBlock.SetVector(Tiling, scale * 0.1f);
            gridSpriteRenderer.SetPropertyBlock(propertyBlock);
            gridSpriteRenderer.sprite = gridData.GetRandomGroundTile();

            vCam.m_Lens.OrthographicSize = gridData.OrthographicSize;
            vCam.GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance = gridData.CameraDistance;

            //set confiner size
            cameraConfiner.gameObject.transform.localScale = scale;

            //spawn blocker colliders
            Transform leftCollider = Instantiate(colliderPrefab, transform, true).transform;
            Transform rightCollider = Instantiate(colliderPrefab, transform, true).transform;
            Transform bottomCollider = Instantiate(colliderPrefab, transform, true).transform;
            Transform topCollider = Instantiate(colliderPrefab, transform, true).transform;

            borderColliders[0] = leftCollider;
            borderColliders[1] = rightCollider ;
            borderColliders[2] = bottomCollider;
            borderColliders[3] = topCollider;

            leftCollider.name = "Left Border";
            rightCollider.name = "Right Border";
            bottomCollider.name = "Bottom Border";
            topCollider.name = "Top Border";

            Vector3 pos = default;
            scale.x = 1;
            scale.y = gridData.OrthographicSize * 2f * gridData.GridY;
            pos.x = -1 * aspectTimesOrthoSize * gridData.GridX - 0.5f;

            leftCollider.position = pos;
            leftCollider.localScale = scale;


            pos.x = aspectTimesOrthoSize * gridData.GridX + 0.5f;
            rightCollider.position = pos;
            rightCollider.localScale = scale;

            pos.x = 0;
            pos.y = -1 * gridData.OrthographicSize * gridData.GridY - 0.5f;
            scale.y = 1;
            scale.x = aspectTimesOrthoSize * 2f * gridData.GridX + 2;
            bottomCollider.position = pos;
            bottomCollider.localScale = scale;

            pos.x = 0f;
            pos.y = gridData.OrthographicSize * gridData.GridY + 0.5f;
            scale.y = 1;
            scale.x = aspectTimesOrthoSize * 2f * gridData.GridX + 2;

            topCollider.position = pos;
            topCollider.localScale = scale;
        }

        private IEnumerator SpawnEnvironmentElements()
        {
            GameObject parent = new GameObject("Environment Elements");
            parent.transform.SetParent(transform);
            
            
            environmentElements = new GameObject[gridData.ElementsToSpawn];
            float aspectTimesOrthoSize = cam.aspect * gridData.OrthographicSize;

            Vector3 pos = default;
            
            for (int i = 0; i < gridData.ElementsToSpawn; i++)
            {
                pos.x = Random.Range(-Mathf.FloorToInt(aspectTimesOrthoSize * gridData.GridX) + 2, Mathf.FloorToInt(aspectTimesOrthoSize * gridData.GridX) - 1);
                pos.y = Random.Range(-Mathf.FloorToInt(gridData.OrthographicSize * gridData.GridY) + 2, Mathf.FloorToInt(gridData.OrthographicSize * gridData.GridY) - 1);
                
                environmentElements[i] = Instantiate(gridData.GetRandomEnvironmentElement(), pos, Quaternion.identity);
                yield return null;
                
                environmentElements[i].transform.SetParent(parent.transform);

                yield return null;
            }
            
        }

    }
}
