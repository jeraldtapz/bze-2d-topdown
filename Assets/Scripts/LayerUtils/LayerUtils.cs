﻿using UnityEngine;

namespace Utilities
{
	public static class LayerUtils
	{
		public static bool IsOnInvalidLayer(LayerMask layerMask, int layer)
		{
			int targetLayer = layerMask.value;
			int otherLayer = 1;
			for (int i = 0; i < layer; i++)
			{
				otherLayer *= 2;
			}

			int result = targetLayer & otherLayer;

			if (result != otherLayer || result == 0)
				return true;
			return false;
		}
	}
}