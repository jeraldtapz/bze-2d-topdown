using TMPro;
using UnityEngine;

namespace Utilities
{
    public class FPSCounter : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI fpsText = null;
        
        public int FPS { get; private set; }

        private float timer = 0f;

        private void Start()
        {
            FPS = (int)(1f / Time.unscaledDeltaTime);
            fpsText.text = FPS.ToString();
        }

        private void Update()
        {
            timer += Time.deltaTime;

            if (timer > 1f)
            {
                FPS = (int)(1f / Time.unscaledDeltaTime);
                fpsText.text = FPS.ToString();
                timer = 0f;
            }
        }
    }
}
