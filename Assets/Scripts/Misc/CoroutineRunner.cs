﻿using UnityEngine;

namespace Utilities
{
	public class CoroutineRunner : MonoBehaviour
	{
		public static CoroutineRunner Instance { get; private set; }
		
		private void Awake()
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}
}