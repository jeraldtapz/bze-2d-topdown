﻿using UnityEngine;

namespace Utilities
{
	public static class HashUtilities
	{
		public static int ToAnimatorHash(this string str)
		{
			return Animator.StringToHash(str);
		}

		public static int ToShaderID(this string str)
		{
			return Shader.PropertyToID(str);
		}
	}
}